package demo.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "organizations")
public class Organization implements Serializable {

    private static final long serialVersionUID = -4146530719344921932L;

    private long id;
    private String name;
    private Set<Application> applications;

    public Organization() {
    }

    public Organization(long id) {
        this.id = id;
    }

    public Organization(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Organization(long id, String name, Set<Application> applications) {
        this.id = id;
        this.name = name;
        this.applications = applications;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "org_id", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Application.class)
    // @JoinTable(name = "ORGANIZATION_APPLICATION", joinColumns = @JoinColumn(name = "ORGANIZATION_ID", nullable = true) ,
    // inverseJoinColumns = @JoinColumn(name = "APPLICATOIN_ID") )
    // @Column(name = "app_id")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "organization")
    public Set<Application> getApplications() {
        return applications;
    }

    public void setApplications(Set<Application> applications) {
        this.applications = applications;
    }

}
