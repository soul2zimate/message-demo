package demo.models;

import org.springframework.data.repository.CrudRepository;

/**
 * @author wangc
 *
 */
public interface SubscriberDao extends CrudRepository<Subscriber, Long> {

    public Subscriber findSubscriberById(long id);

}
