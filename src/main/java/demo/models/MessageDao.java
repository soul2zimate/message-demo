package demo.models;

import org.springframework.data.repository.CrudRepository;

/**
 * @author wangc
 *
 */
public interface MessageDao extends CrudRepository<Message, Long> {

    public Message findMessageById(long id);

}