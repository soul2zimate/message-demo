package demo.models;

import org.springframework.data.repository.CrudRepository;

/**
 * @author wangc
 *
 */
public interface ApplicationDao extends CrudRepository<Application, Long> {

    public Application findApplicationById(long id);

    public Application findApplicationByName(String name);

}
