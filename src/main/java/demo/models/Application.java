package demo.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author wangc
 *
 */
@Entity
@Table(name = "applications")
public class Application implements Serializable {

    private static final long serialVersionUID = -3366324555561265833L;

    private long id;
    private String name;
    private Organization organization;
    private Set<OfficialAccount> officialAccounts;

    public Application() {
    }

    public Application(long id) {
        this.id = id;
    }

    public Application(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Application(long id, String name, Organization organization) {
        this.id = id;
        this.name = name;
        this.organization = organization;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "app_id", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "org_id")
    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "application_officialaccount")
    public Set<OfficialAccount> getOfficialAccounts() {
        return officialAccounts;
    }

    public void setOfficialAccounts(Set<OfficialAccount> officialAccounts) {
        this.officialAccounts = officialAccounts;
    }
}
