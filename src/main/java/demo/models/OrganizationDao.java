package demo.models;

import org.springframework.data.repository.CrudRepository;

/**
 * @author wangc
 *
 */
public interface OrganizationDao extends CrudRepository<Organization, Long> {

    public Organization findOrganizationById(long id);

    public Organization findOrganizationByName(String name);

}
