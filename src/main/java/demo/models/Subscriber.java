package demo.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * @author wangc
 *
 */
@Entity
@Table(name = "subscribers")
public class Subscriber implements Serializable {

    private static final long serialVersionUID = -3351338756965535969L;

    private long id;
    private String loginId;
    private String name;
    private String description;

    private Set<OfficialAccount> officialAccounts;

    public Subscriber() {

    }

    public Subscriber(long id, String loginId, String name) {
        this.id = id;
        this.loginId = loginId;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subsriber_id", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "loginid", unique = true, nullable = false)
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany(mappedBy = "subcribers")
    public Set<OfficialAccount> getOfficialAccounts() {
        return officialAccounts;
    }

    public void setOfficialAccounts(Set<OfficialAccount> officialAccounts) {
        this.officialAccounts = officialAccounts;
    }

}
