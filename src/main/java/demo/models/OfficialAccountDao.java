package demo.models;

import org.springframework.data.repository.CrudRepository;

/**
 * @author wangc
 *
 */
public interface OfficialAccountDao extends CrudRepository<OfficialAccount, Long> {

    public OfficialAccount findOfficialAccountById(long id);

}
