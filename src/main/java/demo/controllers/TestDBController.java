package demo.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import demo.models.Organization;
import demo.models.OrganizationDao;

/**
 * @author wangc
 *
 */
@Controller
public class TestDBController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OrganizationDao organizationDao;

    @RequestMapping("/testdb")
    @ResponseBody
    public String index() {

        return "<p>/insert?id=[id]&name=[name] create a new organization with an auto-generated id and name as passed values.</p>"
                + "<p>/delete?id=[id]: delete the organization with the passed id.</p>"
                + "<p>/findorganizationbyid?id=[id]: retrieve the name for the organization with the given id.</p>"
                + "<p>/findOrganizationByName?name=[name]: retrieve the id for the organization with the given name.</p>"
                + "<p>/update?id=[id]&name=[name]: update the name for the organization indentified by the given id.</p>"
                + "<p><a href='/testdb/findall'>List all organizations in table</a></p>" + "Proudly handcrafted by "
                + "<a href='https://github.com/soul2zimate'>soul2zimate</a> :)";
    }

    @RequestMapping("/testdb/insert")
    @ResponseBody
    public String insert(long id, String name) {
        Organization org = new Organization(id, name, null);
        try {
            organizationDao.save(org);
        } catch (Exception ex) {
            return "Error creating the organization: " + ex.toString();
        }
        return "Organization succesfully created! (id = " + org.getId() + ")";
    }

    @RequestMapping("/testdb/delete")
    @ResponseBody
    public String delete(long id) {
        try {
            organizationDao.delete(id);
        } catch (Exception ex) {
            return "Error deleting the organization:" + ex.toString();
        }
        return "Organization succesfully deleted!";
    }

    @RequestMapping("/testdb/findall")
    @ResponseBody
    public List<String> findAll() {
        List<String> orgs = new ArrayList<String>();
        try {
            organizationDao.findAll().forEach(org -> orgs.add(org.getName()));
        } catch (Exception ex) {
            log.error("Error creating the organization: " + ex.toString());
        }
        return orgs;
    }

    @RequestMapping("/testdb/update")
    @ResponseBody
    public String update(long id, String name) {
        try {
            Organization org = organizationDao.findOne(id);
            org.setName(name);
            organizationDao.save(org);
        } catch (Exception ex) {
            return "Error updating the organization: " + ex.toString();
        }
        return "Organization succesfully updated!";
    }

    @RequestMapping("/testdb/findorganizationbyid")
    @ResponseBody
    public String findOrganizationById(long id) {
        String orgName = null;
        try {
            Organization org = organizationDao.findOrganizationById(id);
            if (org != null) {
                System.out.println(org.getId());
                System.out.println(org.getName());
                System.out.println(org.getApplications());
                orgName = org.getName();
            } else {
                System.out.println("org is null");
            }
        } catch (Exception ex) {
            log.info("Organiztion with id : " + id + " not found due to " + ex);
        }
        return "Organiztion " + orgName + " is found by id : " + id;

    }

    @RequestMapping("/testdb/findorganizationbyname")
    @ResponseBody
    public String findOrganizationByName(String name) {
        long orgId = 0;
        try {
            Organization org = organizationDao.findOrganizationByName(name);
            orgId = org.getId();
        } catch (Exception ex) {
            log.info("Organiztion with name : " + name + " not found");
        }
        return "Organiztion Id" + orgId + " is found by name : " + name;

    }
}